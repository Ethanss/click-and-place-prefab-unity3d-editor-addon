//Ethan Alexander Shulman 2015
// https://twitter.com/EthanShulman   http://etahn.com/

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class PrefabPlacer : EditorWindow {

	private GameObject prefab;
	private bool randomYaw = true;
	//private bool colliders = false;
	private float tiltRange = 0.0f;
	private Vector3 minScale = Vector3.one,maxScale = Vector3.one;
	private Vector3 rotation = Vector3.zero;
	private float offsetRange = 0.5f;
	private LayerMask layerMask = -1;


	private const float PAINT_DISTANCE = 200.0f;


	[MenuItem ("Window/Click and Place Prefab")]
	static void Init () {
		EditorWindow window = ScriptableObject.CreateInstance<PrefabPlacer>();
		window.Show();
	}


	void OnEnable() {
		SceneView.onSceneGUIDelegate += OnSceneGUI;
	}
	void OnDisable() {
		SceneView.onSceneGUIDelegate -= OnSceneGUI;
	}


	void OnGUI() {
		//EditorGUILayout.LabelField();
		prefab = EditorGUILayout.ObjectField("Prefab",prefab, typeof(GameObject), true) as GameObject;
		randomYaw = EditorGUILayout.Toggle ("Random Yaw",randomYaw);
		tiltRange = EditorGUILayout.Slider("Random Tilt Range",tiltRange,0.0f,1.0f);
		minScale = EditorGUILayout.Vector3Field("Min Scale",minScale);
		maxScale = EditorGUILayout.Vector3Field("Max Scale",maxScale);
		offsetRange = EditorGUILayout.FloatField("Offset",offsetRange);
		rotation = EditorGUILayout.Vector3Field ("Rotation",rotation);
		layerMask = LayerMaskField("Layer Mask",layerMask);
		//colliders = EditorGUILayout.Toggle("Paint Colliders",colliders);
	}


	public void OnSceneGUI(SceneView sv) {
		Event e = Event.current;

		if ((e.type == EventType.MouseUp || e.type == EventType.MouseDown || e.type == EventType.MouseDrag) && e.button == 0) {
			if (prefab == null || Camera.current == null) {
				e.Use(); 
				return;
			}

			Vector2 mouse = e.mousePosition;
			mouse.y = Camera.current.pixelHeight-mouse.y;

			Ray ray = Camera.current.ScreenPointToRay(mouse);
			RaycastHit rayHit = new RaycastHit();
			bool hasHit = false;

			//if (colliders) {
				hasHit = Physics.Raycast(ray,out rayHit,PAINT_DISTANCE,layerMask);
			/*} else {
				RaycastHit[] hits;
				hits = Physics.RaycastAll(ray,PAINT_DISTANCE,layerMask);

				hasHit = hits.Length > 0;
				if (hasHit) {

					int hitId = 0;
					float shortestDist = PAINT_DISTANCE;

					for (int i = 0; i < hits.Length; i++) {
						if (hits[i].distance < shortestDist) {
							hitId = i;
							shortestDist = hits[i].distance;
						}
					}

					rayHit = hits[hitId];

				}
			}*/

			if (hasHit) {
				Quaternion rotQuat = Quaternion.Inverse (Quaternion.Euler(rotation+Vector3.up*Random.value*(randomYaw?720.0f:0.0f)));
				GameObject go = GameObject.Instantiate(prefab,
				                                       rayHit.point+rayHit.normal*offsetRange, 
				                                       Quaternion.LookRotation(Vector3.Normalize (rayHit.normal + new Vector3(Random.value-0.5f,Random.value-0.5f,Random.value-0.5f)*tiltRange))*rotQuat)
														as GameObject;

				Vector3 sca = maxScale-minScale;
				sca.x *= Random.value;
				sca.y *= Random.value;
				sca.z *= Random.value;
				go.transform.localScale = minScale + sca;

				Undo.RegisterCreatedObjectUndo (go, "Created game object");
			}

			e.Use(); 

		}

	}

	//Thanks to FlyingOstriche from Unity3D forums for the LayerMaskField
	//http://answers.unity3d.com/questions/42996/how-to-create-layermask-field-in-a-custom-editorwi.html
	static LayerMask LayerMaskField( string label, LayerMask layerMask) {
		List<string> layers = new List<string>();
		List<int> layerNumbers = new List<int>();
		
		for (int i = 0; i < 32; i++) {
			string layerName = LayerMask.LayerToName(i);
			if (layerName != "") {
				layers.Add(layerName);
				layerNumbers.Add(i);
			}
		}
		int maskWithoutEmpty = 0;
		for (int i = 0; i < layerNumbers.Count; i++) {
			if (((1 << layerNumbers[i]) & layerMask.value) > 0)
				maskWithoutEmpty |= (1 << i);
		}
		maskWithoutEmpty = EditorGUILayout.MaskField( label, maskWithoutEmpty, layers.ToArray());
		int mask = 0;
		for (int i = 0; i < layerNumbers.Count; i++) {
			if ((maskWithoutEmpty & (1 << i)) > 0)
				mask |= (1 << layerNumbers[i]);
		}
		layerMask.value = mask;
		return layerMask;
	}


}
