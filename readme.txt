Small and simple editor window for placing prefabs on any colliders in your scene.


How to video: https://www.youtube.com/watch?v=nS_HS18_5lc

How to use:
- Go to the window menu in the toolbar and open the Click and Place Prefab window.
- Select a prefab.
- Click to place the prefab.



Options are described below:

Prefab = GameObject to place.


Random Yaw = bool whether or not to rotate on the y axis.


Random Tilt Range = float(0 - 1) on how much to tilt the object.


Min Scale/MaxScale = Object scale will be a random vector between Min Scale and Max Scale.


Offset Range = How high off the ground to place the prefab.


Rotation = General rotation to apply.


Layer Mask = Layer mask of the colliders able to place the prefab on.




Ethan Alexander Shulman 2015
http://etahn.com/     https://twitter.com/EthanShulman

